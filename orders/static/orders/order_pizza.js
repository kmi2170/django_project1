document.addEventListener('DOMContentLoaded', () => {

    document.querySelectorAll('.pizza').forEach((item) => {
        if (item.checked) {
            showTopping();
        }
        item.addEventListener('change', () => showTopping());
    });

    document.querySelectorAll('input[name="size"]').forEach((item) => {
        if (item.checked) {
            priceSize(item);
        }
        item.addEventListener('change', () => priceSize(item));
    });

    document.querySelectorAll('input').forEach((item) => {
        item.addEventListener('change', () => {
            document.querySelector('#message').innerHTML = "";
        });
    });

    /*
    document.querySelector('button').addEventListener('click', () => {
        return validateForm();
    });
    */
});

function showTopping() {
    if (document.querySelector('#topping_1').checked) {
        document.querySelector('#option_1').hidden = false;
        document.querySelector('#option_2').hidden = true;
        document.querySelector('#option_3').hidden = true;
        document.querySelector('#option_2 select[name="topping_1"]').value = "";
        document.querySelector('#option_2 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_1"]').value = "";
        document.querySelector('#option_3 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_3"]').value = "";
        document.querySelector('#option_1 select[name="topping_1"]').disabled = false;
        document.querySelector('#option_2 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_3"]').disabled = true;
    } else if (document.querySelector('#topping_2').checked) {
        document.querySelector('#option_1').hidden = true;
        document.querySelector('#option_2').hidden = false;
        document.querySelector('#option_3').hidden = true;
        document.querySelector('#option_1 select[name="topping_1"]').value = "";
        document.querySelector('#option_3 select[name="topping_1"]').value = "";
        document.querySelector('#option_3 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_3"]').value = "";
        document.querySelector('#option_1 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_1"]').disabled = false;
        document.querySelector('#option_2 select[name="topping_2"]').disabled = false;
        document.querySelector('#option_3 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_3"]').disabled = true;
    } else if (document.querySelector('#topping_3').checked) {
        document.querySelector('#option_1').hidden = true;
        document.querySelector('#option_2').hidden = true;
        document.querySelector('#option_3').hidden = false;
        document.querySelector('#option_2 select[name="topping_1"]').value = "";
        document.querySelector('#option_2 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_1"]').value = "";
        document.querySelector('#option_1 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_1"]').disabled = false;
        document.querySelector('#option_3 select[name="topping_2"]').disabled = false;
        document.querySelector('#option_3 select[name="topping_3"]').disabled = false;
    } else {
        document.querySelector('#option_1').hidden = true;
        document.querySelector('#option_2').hidden = true;
        document.querySelector('#option_3').hidden = true;
        document.querySelector('#option_1 select[name="topping_1"]').value = "";
        document.querySelector('#option_2 select[name="topping_1"]').value = "";
        document.querySelector('#option_2 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_1"]').value = "";
        document.querySelector('#option_3 select[name="topping_2"]').value = "";
        document.querySelector('#option_3 select[name="topping_3"]').value = "";
        document.querySelector('#option_1 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_2 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_1"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_2"]').disabled = true;
        document.querySelector('#option_3 select[name="topping_3"]').disabled = true;
    }
}


function priceSize(item) {
    if (item.value === "small") {
        document.querySelectorAll('.small').forEach((item) => {
            item.hidden = false;
        });
        document.querySelectorAll('.large').forEach((item) => {
            item.hidden = true;
        });
    } else {
        document.querySelectorAll('.small').forEach((item) => {
            item.hidden = true;
        });
        document.querySelectorAll('.large').forEach((item) => {
            item.hidden = false;
        });
    }
}


function validateForm() {
    if (document.querySelector('#topping_1').checked) {
        const item = document.querySelector('#option_1 select[name="topping_1"]');
        if (item.value == "") {
            const message = "Choose One Option";
            document.querySelector('#message').innerHTML = message;
            return false;
        } else {
            return true;
        }
        return false;
    } else if (document.querySelector('#topping_2').checked) {
        const item1 = document.querySelector('#option_2 select[name="topping_1"]');
        const item2 = document.querySelector('#option_2 select[name="topping_2"]');
        if (item1.value == "" || item2.value == "") {
            const message = "Choose Two Options";
            document.querySelector('#message').innerHTML = message;
            return false;
        } if (item1.value == item2.value) {
            const message = "Choose Two Different Options";
            document.querySelector('#message').innerHTML = message;
            return false;
        } else {
            return true;
        }
        return false;
    } else if (document.querySelector('#topping_3').checked) {
        const item1 = document.querySelector('#option_3 select[name="topping_1"]');
        const item2 = document.querySelector('#option_3 select[name="topping_2"]');
        const item3 = document.querySelector('#option_3 select[name="topping_3"]');
        if (item1.value == "" || item2.value == "" || item3.value == "") {
            const message = "Choose Three Options";
            document.querySelector('#message').innerHTML = message;
            return false;
        } if (item1.value == item2.value || item1.value == item3.value || item2.value == item3.value) {
            const message = "Choose Three Different Options";
            document.querySelector('#message').innerHTML = message;
            return false;
        } else {
            return true;
        }
        return false;
    } else {
        return true;
    }
}
