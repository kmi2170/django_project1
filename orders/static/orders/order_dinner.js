document.addEventListener('DOMContentLoaded', () => {

    document.querySelectorAll('input[name="size"]').forEach((item) => {
        if (item.checked) {
            priceSize(item);
        }
        item.addEventListener('change', () => priceSize(item));
    });
});

function validateForm() {
        //console.log('validating');
        let check = false;
        document.querySelectorAll('input[name="id"]').forEach((item) => {
            if (item.checked){
                check = true
            }
        });
        if (check === true) {
            return true;
        } else {
            message = "Please Choose Item"
            document.querySelector('#message').innerHTML = message;
            return false;
        }
}

function priceSize(item) {
    if (item.value === "small") {
        document.querySelectorAll('.small').forEach((item) => {
            item.hidden = false;
        });
        document.querySelectorAll('.large').forEach((item) => {
            item.hidden = true;
        });
    } else {
        document.querySelectorAll('.small').forEach((item) => {
            item.hidden = true;
        });
        document.querySelectorAll('.large').forEach((item) => {
            item.hidden = false;
        });
    }
}
