# Generated by Django 3.0.3 on 2020-03-18 05:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0022_auto_20200318_0519'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Purchased',
            new_name='Order',
        ),
    ]
