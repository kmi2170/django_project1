# Generated by Django 3.0.3 on 2020-03-18 03:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0014_auto_20200318_0229'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchased',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_id', models.IntegerField()),
                ('order_date', models.DateTimeField()),
                ('username', models.CharField(max_length=32)),
                ('type', models.CharField(max_length=32)),
                ('menu', models.CharField(max_length=64)),
                ('size', models.CharField(max_length=16)),
                ('price', models.IntegerField()),
                ('price_str', models.CharField(max_length=16)),
                ('quantity', models.IntegerField()),
                ('topping_1', models.CharField(max_length=32)),
                ('topping_2', models.CharField(max_length=32)),
                ('topping_3', models.CharField(max_length=32)),
            ],
        ),
    ]
