# Generated by Django 3.0.3 on 2020-03-17 18:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20200317_0519'),
    ]

    operations = [
        migrations.RenameField(
            model_name='menu_main',
            old_name='price_char_large',
            new_name='price_str_large',
        ),
        migrations.RenameField(
            model_name='menu_main',
            old_name='price_char_small',
            new_name='price_str_small',
        ),
        migrations.RenameField(
            model_name='menu_others',
            old_name='price_char',
            new_name='price_str',
        ),
    ]
