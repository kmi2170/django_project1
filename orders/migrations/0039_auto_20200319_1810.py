# Generated by Django 3.0.3 on 2020-03-19 18:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0038_auto_20200319_1809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='purchased',
            field=models.IntegerField(default=0),
        ),
    ]
