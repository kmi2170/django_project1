# Generated by Django 3.0.3 on 2020-03-18 04:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0018_auto_20200318_0414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='order_date',
            field=models.CharField(max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='order_id',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='purchased',
            field=models.IntegerField(null=True),
        ),
    ]
