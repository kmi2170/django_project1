from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Menu_all(models.Model):
    type = models.CharField(max_length=32)
    menu = models.CharField(max_length=32)
    price = models.IntegerField(null=True)
    price_small = models.IntegerField(null=True)
    price_large = models.IntegerField(null=True)
    price_str = models.CharField(max_length=16, null=True)
    price_str_small = models.CharField(max_length=16, null=True)
    price_str_large = models.CharField(max_length=16, null=True)
    availability = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.id} {self.type} {self.menu}"


class Menu_main(models.Model):
    type = models.CharField(max_length=32)
    menu = models.CharField(max_length=32)
    price_small = models.IntegerField()
    price_large = models.IntegerField()
    price_str_small = models.CharField(max_length=16)
    price_str_large = models.CharField(max_length=16)


class Menu_others(models.Model):
    type = models.CharField(max_length=32)
    menu = models.CharField(max_length=32)
    price = models.IntegerField()
    price_str = models.CharField(max_length=16)


class Menu_toppings(models.Model):
    topping = models.CharField(max_length=32, unique=True)
    availability = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.topping} {self.availability}"


class Order(models.Model):
    username = models.ForeignKey(User, on_delete=models.PROTECT)
    order_date = models.CharField(max_length=32, null=True)
    total_quantity = models.IntegerField()
    total_price_d = models.IntegerField()
    total_price_c = models.IntegerField()

    def __str__(self):
        return f"{self.username} {self.order_date}"


class Cart(models.Model):
    username = models.ForeignKey(User, on_delete=models.PROTECT)
    item = models.ForeignKey(Menu_all, on_delete=models.PROTECT, null=True)
    size = models.CharField(max_length=8, null=True)
    quantity = models.IntegerField()
    topping_1 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_1")
    topping_2 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_2")
    topping_3 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_3")
    purchased = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.PROTECT, null=True)


class Cart2(models.Model):
    username = models.ForeignKey(User, on_delete=models.PROTECT)
    type = models.CharField(max_length=32)
    menu = models.CharField(max_length=32)
    size = models.CharField(max_length=16)
    price = models.IntegerField(null=True)
    price_str = models.CharField(max_length=16)
    quantity = models.IntegerField()
    total_price = models.IntegerField(null=True)
    total_price_d = models.IntegerField(null=True)
    total_price_c = models.IntegerField(null=True)
    topping_1 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_12")
    topping_2 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_22")
    topping_3 = models.ForeignKey(Menu_toppings, on_delete=models.PROTECT,
    null=True, related_name="topping_32")
    purchased = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.PROTECT, null=True)

    #username = models.CharField(max_length=32)
    #topping_1 = models.CharField(max_length=32, null=True)
    #topping_2 = models.CharField(max_length=32, null=True)
    #topping_3 = models.CharField(max_length=32, null=True)
#    order_id = models.IntegerField(null=True)

    # order_id = models.IntegerField()
    #order_date = models.DateTimeField()
    # username = models.CharField(max_length=32)
    # type = models.CharField(max_length=32)
    # menu = models.CharField(max_length=64)
    # size = models.CharField(max_length=16)
    # price = models.IntegerField()
    # price_str = models.CharField(max_length=16)
    # quantity = models.IntegerField()
    # topping_1 = models.CharField(max_length=32)
    # topping_2 = models.CharField(max_length=32)
    # topping_3 = models.CharField(max_length=32)


# class Users(models.Model):
    # username = models.CharField(max_length=32)
    # password = models.CharField(max_length=32)
    # firstname = models.CharField(max_length=64)
    # lastname = models.CharField(max_length=64)
    # email = models.EmailField(max_length=64)
