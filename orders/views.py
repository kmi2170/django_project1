from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Menu_all, Menu_main, Menu_others, Menu_toppings, Cart, Order
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime
from pytz import timezone


def index(request):

    regular_pizza = Menu_all.objects.filter(
        type="Regular Pizza").filter(availability=1)
    sicilian_pizza = Menu_all.objects.filter(
        type="Sicilian Pizza").filter(availability=1)
    salad = Menu_all.objects.filter(type="Salads").filter(availability=1)
    pasta = Menu_all.objects.filter(type="Pasta").filter(availability=1)
    dinner_platters = Menu_all.objects.filter(
        type="Dinner Platters").filter(availability=1)
    toppings = Menu_toppings.objects.filter(availability=1)

    context = {
        "regular_pizza": regular_pizza,
        "sicilian_pizza": sicilian_pizza,
        "salad": salad,
        "pasta": pasta,
        "dinner_platters": dinner_platters,
        "toppings": toppings,
    }

    # print(request.user.is_authenticated)
    # print(request.user.username)
    if not request.user.is_authenticated:
        return render(request, "orders/index.html", context)
    else:
        context["username"] = request.user.username
        return render(request, "orders/index.html", context)


def change(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    cid = int(request.POST["cid"])
    quantity = int(request.POST["quantity"])
    obj = Cart.objects.get(pk=cid)
    obj.quantity = quantity
    obj.save()

    print(f"cid: {cid}")
    print(f"quantity: {quantity}")

    return HttpResponseRedirect(reverse("view_cart"))


def delete_confirm(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    username = request.user.username
    cid = int(request.POST["cid"])
    obj = Cart.objects.get(pk=cid)

    type = obj.item.type
    menu = obj.item.menu
    size = obj.size
    quantity = obj.quantity
    if size == "small":
        price_str = obj.item.price_str_small
    elif size == "large":
        price_str = obj.item.price_str_large
    else:
        price_str = obj.item.price_str

    try:
        topping_1 = obj.topping_1.topping
    except:
        topping_1 = None
    try:
        topping_2 = obj.topping_2.topping
    except:
        topping_2 = None
    try:
        topping_3 = obj.topping_3.topping
    except:
        topping_3 = None
    toppings = [topping_1, topping_2, topping_3]

    item = {
        "cid": cid,
        "username": username,
        "type": type,
        "muenu": menu,
        "size": size,
        "quantity": quantity,
        "price_str": price_str,
        "toppings": toppings,
    }

    return render(request, "orders/delete_confirm.html", item)


def delete(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    cid = int(request.POST["cid"])
    obj = Cart.objects.get(pk=cid)
    print("delete")
    obj.delete()
    return HttpResponseRedirect(reverse("view_cart"))


def order_success(request):
    return render(request, "orders/order_success.html")


def place_order(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    username = request.user.username
    username = User.objects.get(username=username)
    objs = Cart.objects.filter(username=username).filter(purchased=0)

    total_quantity = 0
    total_price = 0
    for obj in objs:
        size = obj.size
        if size == "small":
            price = obj.item.price_small
        elif size == "large":
            price = obj.item.price_large
        else:
            price = obj.item.price
        price = int(price)
        quantity = int(obj.quantity)
        total_quantity += quantity
        total_price += price * quantity
        obj.purchased = 1
    #    obj.save()

    (total_price_d, total_price_c) = show_total_price(total_price, 1)
    time = datetime.now(timezone("US/Pacific"))
    time = time.strftime('%Y/%m/%d %H:%M:%S')

    pobj = Order(username=username, order_date=time, total_quantity=total_quantity,
                 total_price_d=total_price_d, total_price_c=total_price_c)
    pobj.save()

    for obj in objs:
        obj.order = pobj
        obj.save()

#    return HttpResponseRedirect(reverse("view_purchased"))
    return HttpResponseRedirect(reverse("order_success"))


def view_purchased(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    username = request.user.username
    username = User.objects.get(username=username)

    objs = Order.objects.filter(username=username)
    array = []
    array_cart = []
    for obj in objs:
        order_id = obj.pk
        order_date = obj.order_date
        total_quantity = obj.total_quantity
        total_price_d = obj.total_price_d
        total_price_c = obj.total_price_c

        cobjs = Cart.objects.filter(order_id=order_id)
        array_cart = []
        for cobj in cobjs:
            menu = cobj.item.menu
            type = cobj.item.type
            size = cobj.size
            quantity = cobj.quantity
            if size == "small":
                price = cobj.item.price_small
                price_str = cobj.item.price_str_small
            elif size == "large":
                price = cobj.item.price_large
                price_str = cobj.item.price_str_large
            else:
                price = cobj.item.price
                price_str = cobj.item.price_str

            price = int(price)
            quantity = int(cobj.quantity)
            (sub_total_price_d, sub_total_price_c) = show_total_price(price, quantity)

            try:
                topping_1 = cobj.topping_1.topping
            except:
                topping_1 = None
            try:
                topping_2 = cobj.topping_2.topping
            except:
                topping_2 = None
            try:
                topping_3 = cobj.topping_3.topping
            except:
                topping_3 = None
            toppings = [topping_1, topping_2, topping_3]

            array_cart.append({
                "type": type,
                "menu": menu,
                "size": size,
                "quantity": quantity,
                "price_str": price_str, "sub_total_price_d": sub_total_price_d, "sub_total_price_c": sub_total_price_c,                "toppings": toppings,
            })

        your_order = {
            "order_id": order_id,
            "order_date": order_date,
            "purchased": array_cart,
            "total_quantity": total_quantity,
            "total_price_d": total_price_d,
            "total_price_c": total_price_c,
        }
        array.append(your_order)

    your_orders = {
        "username": request.user.username,
        "orders": array,
    }
    # print(array)

    return render(request, "orders/purchased.html", your_orders)
    # return HttpResponse("Order is completed")


def view_cart(request):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    username0 = request.user.username
    username = User.objects.get(username=username0)
    objs = Cart.objects.filter(username=username).filter(purchased=0)

    total_quantity = 0
    total_price = 0
    your_cart = []
    for obj in objs:

        cid = obj.id
        type = obj.item.type
        menu = obj.item.menu
        size = obj.size
        if size == "small":
            price = obj.item.price_small
            price_str = obj.item.price_str_small
        elif size == "large":
            price = obj.item.price_large
            price_str = obj.item.price_str_large
        else:
            price = obj.item.price
            price_str = obj.item.price_str

        price = int(price)
        quantity = int(obj.quantity)
        (sub_total_price_d, sub_total_price_c) = show_total_price(price, quantity)

        try:
            topping_1 = obj.topping_1.topping
        except:
            topping_1 = None
        try:
            topping_2 = obj.topping_2.topping
        except:
            topping_2 = None
        try:
            topping_3 = obj.topping_3.topping
        except:
            topping_3 = None
        toppings = [topping_1, topping_2, topping_3]

        your_cart.append([
            {"cid": cid, "type": type, "menu": menu, "size": size, "quantity": quantity,
             "price_str": price_str, "sub_total_price_d": sub_total_price_d, "sub_total_price_c": sub_total_price_c,
             "toppings": toppings}])

        total_price += price * quantity
        total_quantity += quantity
        print(f"obj.item {obj.item.menu}")

    (total_price_d, total_price_c) = show_total_price(total_price, 1)
    context = {
        "username": username0,
        "your_cart": your_cart,
        "total_quantity": total_quantity,
        "total_price_d": total_price_d,
        "total_price_c": total_price_c,
    }
    return render(request, "orders/cart.html", context)


def add_cart_subs(request):
    if not request.user.is_authenticated or request.method == "GET":
        return HttpResponse("Access Denied. Please Log In")

    username = request.user.username
    username = User.objects.get(username=username)

    id = request.POST["id"]
    quantity = request.POST["quantity"]
    size = request.POST["size"]

    obj = Menu_all.objects.get(pk=id)

    if size == "none":
        price = obj.price
        price_str = obj.price_str
    else:
        if size == "small":
            price = obj.price_small
            price_str = obj.price_str_small
        else:
            price = obj.price_large
            price_str = obj.price_str_large

    c1 = Cart.objects.filter(item=obj).filter(size=size)
    if c1.count() == 1:
        print("duplicate")
        c2 = c1[0]
        quantity_new = int(c2.quantity) + int(quantity)
        c2.quantity = quantity_new
    else:
        print("Not exist")
        c2 = Cart(username=username, item=obj, size=size,
                  quantity=quantity, purchased=0)

    c2.save()

    return HttpResponseRedirect(reverse("view_cart"))


def add_cart(request):
    if not request.user.is_authenticated or request.method == "GET":
        return HttpResponse("Access Denied. Please Log In")

    username = request.user.username
    username = User.objects.get(username=username)

    id = request.POST["id"]
    size = request.POST["size"]
    quantity = request.POST["quantity"]

    obj = Menu_all.objects.get(pk=id)
    print(f"{obj.id}, {obj.type}, {obj.menu}")

    if "topping_1" in request.POST:
        topping_1 = request.POST["topping_1"]
    else:
        topping_1 = None
    if "topping_2" in request.POST:
        topping_2 = request.POST["topping_2"]
    else:
        topping_2 = None
    if "topping_3" in request.POST:
        topping_3 = request.POST["topping_3"]
    else:
        topping_3 = None

    if topping_1 is not None and topping_2 is not None and topping_3 is None:
        array = [topping_1, topping_2]
        array.sort()
        topping_1 = array[0]
        topping_2 = array[1]
    elif topping_1 is not None and topping_2 is not None and topping_3 is not None:
        array = [topping_1, topping_2, topping_3]
        array.sort()
        topping_1 = array[0]
        topping_2 = array[1]
        topping_3 = array[2]

    if topping_1 is not None:
        topping_1 = Menu_toppings.objects.get(topping=topping_1)
    if topping_2 is not None:
        topping_2 = Menu_toppings.objects.get(topping=topping_2)
    if topping_3 is not None:
        topping_3 = Menu_toppings.objects.get(topping=topping_3)

    c1 = Cart.objects.filter(item=obj).filter(size=size) .filter(
        topping_1=topping_1).filter(topping_2=topping_2).filter(topping_3=topping_3)

    if size == "small":
        price = obj.price_small
        price_str = obj.price_str_small
    else:
        price = obj.price_large
        price_str = obj.price_str_large

    if c1.count() == 1:
        print("duplicate")
        c2 = c1[0]
        quantity_new = int(c2.quantity) + int(quantity)
        c2.quantity = quantity_new
    else:
        print("not exist")
        c2 = Cart(username=username, item=obj, size=size, quantity=quantity,
                  topping_1=topping_1, topping_2=topping_2, topping_3=topping_3, purchased=0)

    c2.save()

    return HttpResponseRedirect(reverse("view_cart"))


def order(request, type):
    if not request.user.is_authenticated:
        return HttpResponse("Access Denied. Please Log In")

    if "Pizza" in type:
        items = {
            "username": request.user.username,
            "type": type,
            "pizza": Menu_all.objects.filter(type=type).filter(availability=1),
            "toppings": Menu_toppings.objects.filter(availability=1)
        }
        return render(request, "orders/order_pizza.html", items)

    elif "Salads" in type or "Pasta" in type:
        items = {
            "username": request.user.username,
            "type": type,
            "subs": Menu_all.objects.filter(type=type).filter(availability=1),
        }
        return render(request, "orders/order_subs.html", items)

    elif "Dinner Platters" in type:
        items = {
            "username": request.user.username,
            "type": type,
            "dinner": Menu_main.objects.filter(type=type),
        }
        return render(request, "orders/order_dinner.html", items)


def login_view(request):
    if request.method == "GET":
        return render(request, "orders/login.html")

    username = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        message = "Invalid credentials"
        return render(request, "orders/login.html", {"message": message})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "GET":
        return render(request, "orders/register.html")

    username = request.POST["username"]
    password = request.POST["password"]
    firstname = request.POST["firstname"]
    lastname = request.POST["lastname"]
    email = request.POST["email"]

    if len(username) == 0 or len(password) == 0 or len(firstname) == 0 or len(lastname) == 0 or len(email) == 0:
        message = "All fields are required."
        return render(request, "orders/register.html", {"message": message})

    user = User.objects.create_user(username, email, password)
    user.first_name = firstname
    user.last_name = lastname
    user.save()

    # return render(request, "orders/register_success.html")
    return HttpResponseRedirect(reverse("register_success"))


def register_success(request):
    return render(request, "orders/register_success.html")


def show_total_price(price, quantity):
    total_price = int(price) * int(quantity)
    total_price = str(total_price)
    total_price_d = total_price[:len(total_price) - 2]
    total_price_c = total_price[len(total_price) - 2:]
    return (total_price_d, total_price_c)
