from django.contrib import admin

from .models import Menu_all, Menu_main, Menu_others, Menu_toppings, Cart, Order
# Register your models here.

admin.site.register(Menu_all)
admin.site.register(Menu_main)
admin.site.register(Menu_others)
admin.site.register(Menu_toppings)
admin.site.register(Cart)
admin.site.register(Order)
