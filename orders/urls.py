from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("register", views.register, name="register"),
    path("register_success", views.register_success, name="register_success"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("order/<str:type>", views.order, name="order"),
    path("add_cart", views.add_cart, name="add_cart"),
    path("add_cart_subs", views.add_cart_subs, name="add_cart_subs"),
    path("view_cart", views.view_cart, name="view_cart"),
    path("place_order", views.place_order, name="place_order"),
    path("view_purchased", views.view_purchased, name="view_purchased"),
    path("order_success", views.order_success, name="order_success"),
    path("delete_confirm", views.delete_confirm, name="delete_confirm"),
    path("delete", views.delete, name="delete"),
    path("change", views.change, name="change"),
]
