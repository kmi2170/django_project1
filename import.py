import sqlite3
import csv

conn = sqlite3.connect("db.sqlite3")
cur = conn.cursor()


def menu_topping():
    fh = open("menu_toppings.csv")
    reader = csv.reader(fh)
    for tp in reader:
        cur.execute('''
        INSERT INTO orders_menu_toppings (topping) VALUES (?)
        ''', (tp))
    conn.commit()


def menu_main():
    fh = open("menu_main.csv")
    reader = csv.reader(fh)
    for tp, menu, ps, pl, pcs, pcl in reader:
        cur.execute('''
        INSERT INTO orders_menu_main (type, menu, price_small, price_large,
        price_str_small, price_str_large)
        VALUES (?, ?, ?, ?, ?, ?)
        ''', (tp, menu, ps, pl, pcs, pcl))
    conn.commit()


def menu_all():
    fh = open("menu_main.csv")
    reader = csv.reader(fh)
    availability = 1
    # for tp, menu, ps, pl, pss, psl in reader:
        # cur.execute('''
        # INSERT INTO orders_menu_all (type, menu, price_small, price_large,
        # price_str_small, price_str_large)
        # VALUES (?, ?, ?, ?, ?, ?)
        # ''', (tp, menu, ps, pl, pss, psl))
        # cur.execute('''
        # UPDATE orders_menu_all SET availability=1 ''')
    # conn.commit()

    fh = open("menu_others.csv")
    reader = csv.reader(fh)
    availability = 1
    for tp, menu, p, ps in reader:
        cur.execute('''
        INSERT INTO orders_menu_all (type, menu, price, price_str, availability)
        VALUES (?, ?, ?, ?, ?)
        ''', (tp, menu, p, ps, availability))
    conn.commit()

def menu_others():
    fh = open("menu_others.csv")
    reader = csv.reader(fh)
    for tp, menu, p, pc in reader:
        cur.execute('''
        INSERT INTO orders_menu_others (type, menu, price, price_char)
        VALUES (?, ?, ?, ?)
        ''', (tp, menu, p, pc))
    conn.commit()


#menu_topping()
#menu_main()
#menu_others()
menu_all()
